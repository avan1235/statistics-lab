# Probability and statistics

Probability and Statistics lab course from 2nd year of Computer Science at University of Warsaw, edition 2018

Based on the [materials for 2016](https://bitbucket.org/marcinmucha/rpis2016.git) by Marcin Mucha
